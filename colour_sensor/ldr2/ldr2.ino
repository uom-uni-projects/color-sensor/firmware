  int sensor,minVal, Val[3] , colArray[3] = {9,10,11},  total, disArray[3] = {3,5,6};

float Percent[3];

int i ,readRGB[3] , readMax[3], Domin;                  //  DECLARE VARIABLES

long calibtime,prevtime;                               //  RECORD THE TIME ELAPSED

void setup()
{
  
 Serial.begin(9600); 

   for(i =0 ; i<3 ; i++)
    {
    pinMode(colArray[i],OUTPUT); 
     pinMode(disArray[i],OUTPUT); // SET THE OUTPUT PINS
    }
    
    calibrate();                                      //  RUN THE CALIBRATE FUNCTION

}

void loop()
{

  total = 0 ;

   for( i = 0  ; i < 3 ; i++)                         //  CHECK VALUES IN A LOOP
  {
     prevtime = millis();
     while(millis()-prevtime < 100)                      //  AVOID DELAY
     {
       analogWrite(colArray[i],Val[i]);                      //  WRITE THE CALIBRATED VALUES
       readRGB[i] = 1024 - analogRead(0);
       delay(50);                                 
     }
  
  digitalWrite(colArray[i],0);
   
  prevtime = millis();                                         // RESET TIME
 
  total = total + readRGB[i];
  
  }


  for(i = 0 ; i < 3 ; i ++)
    {
  Percent[i] = readRGB[i]*100.0/total;                     //  PRINT IN THE FORM OF PERCENTAGE
        Serial.print(Percent[i]);
  Serial.print(" %   ");
    }
    
        Serial.println("");
        flsh(Percent[0],Percent[1],Percent[2]);
        delay(100);
//        float maxP,minP;
//        maxP=max(max(Percent[0],Percent[1]),Percent[2]);
//        minP=min(min(Percent[0],Percent[1]),Percent[2]);
//        if maxP/2>minP{
//          minP=0;
//        }
// 


}

/////////////////////////////////##############################################################################################////////////
  
  void calibrate()                                        //   CALIBRATE FUNCTION
  {
   
      for(i=0;i<3;i++)
      {
         while(millis()-calibtime < 100)                    //    FLASH EACH COLOR AT MAX FOR 1 SEC
         {
             analogWrite(colArray[i],255);
             readMax[i] = analogRead(0);                       //    RECORD MAX VALUES
         }                                              
   
   analogWrite(colArray[i],0);
   Serial.println(readMax[i]);
   delay(10);
   
   calibtime = millis();
   
 }
   
   if(readMax[0] < readMax[1] && readMax[0] < readMax[2])      //   GET THE MINIMUM VALUE FROM ARRAY
   
      minVal = readMax[0];
     
     else 
     {
 
       if( readMax[1] < readMax[0] && readMax[1] < readMax[2])
       
       minVal = readMax[1];
      
  
      else
    
      minVal = readMax[2];
    
   }
  
  
    for(i = 0 ; i < 3 ; i++)
  {
  
       analogWrite(colArray[i],10);
       sensor = analogRead(0);                                    // START CALIBRATION
       delay(100);
   
    while ( sensor - minVal <= -1 || sensor - minVal >= 1 )  //    GET THE DIFFERENCE BETWEEN CURRENT VALUE AND THRESHOLD
       {
         sensor =analogRead(0);
         
         if( sensor > minVal )                                       //   INCREASE OR DECREASE THE VALUE TO EQUALIZE THE BRIGHTNESS
             Val[i]--;
             
             else
             Val[i]++;
             
             Serial.print(analogRead(0));
             Serial.print("      ");
             Serial.println(minVal);
   
             delay(50);
   
             Val[i] = constrain(Val[i],0,255);                 //   CONSTRAIN THE VALUE B/W  0  -- 255
             analogWrite(colArray[i],Val[i]);
       }
 
 analogWrite(colArray[i],0);
 delay(50);
 
 }
 
  }

void flsh(int r,int g, int b){
  int R= r*255/100;
  int G= g*255/100;
  int B= b*255/100;
  //int maxValue=0;


  /*maxValue=max(max(r,g),b);
if (maxValue==r){
  analogWrite(disArray[0],255);
  analogWrite(disArray[1],0);
  analogWrite(disArray[2],0);
  }
  if (maxValue==g){
  analogWrite(disArray[0],0);
  analogWrite(disArray[1],255);
  analogWrite(disArray[2],0);
  }
  if (maxValue==b){
  analogWrite(disArray[0],0);
  analogWrite(disArray[1],0);
  analogWrite(disArray[2],255);
  }
}*/


if (r>40 || g>40 || b >40 ){
  if (r>40){
  analogWrite(disArray[0],255);
  analogWrite(disArray[1],0);
  analogWrite(disArray[2],0);    
  }
 else  if (g>40){
     analogWrite(disArray[0],0);
  analogWrite(disArray[1],255);
  analogWrite(disArray[2],0);
  }  
 else if (b>40){
     analogWrite(disArray[0],0);
  analogWrite(disArray[1],0);
  analogWrite(disArray[2],255);
  }  
}
    
  /*
    }*/
 else if (r<20 || g<20 || b <20 ) {
     if (r<20){
     analogWrite(disArray[0],0);
  analogWrite(disArray[1],G);
  analogWrite(disArray[2],B); 
    }
    if (g<20){
     analogWrite(disArray[0],R);
  analogWrite(disArray[1],0);
  analogWrite(disArray[2],B); 
    }
    if (b<20){
     analogWrite(disArray[0],R);
  analogWrite(disArray[1],G);
  analogWrite(disArray[2],0); 
    }
  }

/*else if(33<=r<=35 && 33<=g<=35 && 31<=g<=33){
analogWrite(disArray[0],255);
analogWrite(disArray[1],255);
analogWrite(disArray[2],255);*/

else if (20<r<40 && 20<g<40 && 20<b<40 ){
    analogWrite(disArray[0],R);
  analogWrite(disArray[1],G);
  analogWrite(disArray[2],B);  
}

Serial.println(Val[1]);

}

/*else {
  analogWrite(disArray[0],R*2);
  analogWrite(disArray[1],G*2);
  analogWrite(disArray[2],B*2);
  }
}
//Serial.println(G*2);

  int minValue,maxValue;
  minValue=min(min(r,g),b);
  maxValue=max(max(r,g),b);
  if (minValue<maxValue/2){
    if (minValue==r){
      r=0;
    }
    else if (minValue==g){
      g=0;
    }
    else{
      b=0;
    }
  }
  r = map(r,minValue,maxValue,0,255);
//  g = constrain(g,25,40);
  g = map(g,minValue,maxValue,0,255);
//  b = constrain(b,25,40M);
  b = map(b,minValue,maxValue,0,255);
  analogWrite(disArray[0],r);
  analogWrite(disArray[1],g);
  analogWrite(disArray[2],b);
  Serial.print(r);
  Serial.print(" ");
  Serial.print(g);
  Serial.print(" ");
  Serial.print(b);
  Serial.println(" ");
 // }*/

